using System;

namespace armasync.lib
{
    public static class Quicky
    {
        private static readonly string[] _byteUnits = new[]{"B", "KB", "MB", "GB", "TB"};

        public static string FormatBytes(long size, int decimalPlaces=1)
        {
            double dsize = size;
            for (int i=0; i<_byteUnits.Length; ++i)
            {
                if (dsize < 1024.0)
                    return string.Format ("{0} {1}", dsize.ToString("N" + decimalPlaces), _byteUnits [i]);
                dsize /= 1024.0;
            }
            return string.Format ("{0} {1}", size, _byteUnits[_byteUnits.Length-1]); //last resort
        }

        public static void WriteProgress(long curr, long total, long speed, int indent=0)
        {
            Console.Write ("\r");
            int len = Console.BufferWidth - indent;

            int progLen = len / 3;
            int completeLen = (int)(curr * progLen / total);

            if (indent > 0)
                Console.Write(new string(' ', indent));

            Console.Write ("{0:00}% ", curr * 100 / total);

            Console.Write ("[");
            for (int i=0; i<completeLen; ++i)
                Console.Write ("=");
            for (int i=0; i<progLen - completeLen; ++i)
                Console.Write (" ");
            Console.Write ("]");

            Console.Write(" ");

            Console.Write ("{0} / {1}", FormatBytes (curr), FormatBytes (total));
            Console.Write ("; ");

            Console.Write ("{0}/s", FormatBytes (speed));

            //Clear the rest of the line
            for (int i = Console.CursorLeft; i < Console.BufferWidth-1; ++i)
                Console.Write(" ");
        }

        public static void ClearCurrentLine(string with = null)
        {
            Console.Write("\r");
            for (int i = 0; i < Console.BufferWidth-1; ++i)
                Console.Write(" ");
            Console.Write("\r");
            if (!string.IsNullOrEmpty(with))
                Console.Write(with);
        }
    }
}

