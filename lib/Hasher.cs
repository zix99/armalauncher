﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace armasync.lib
{
    public static class Hasher
    {
        public static string ComputeFileSha(string filePath)
        {
            using (var stream = File.OpenRead(filePath))
            {
                return ComputeSha(stream);
            }
        }

        public static string ComputeSha(Stream stream)
        {
            using (var sha = SHA1.Create())
            {
                byte[] hashBytes = sha.ComputeHash(stream);
                return GetHexString(hashBytes);
            }
        }

        public static string GetHexString(byte[] bytes)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; ++i)
            {
                sb.Append(bytes[i].ToString("x2"));
            }
            return sb.ToString();
        }

    }
}
