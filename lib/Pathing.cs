﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace armasync.lib
{
    public static class Pathing
    {
        public static string Normalize(string path)
        {
            return path.Replace('/', Path.DirectorySeparatorChar);
        }

        public static string Sanitize(string path)
        {
            return path.Replace("..", string.Empty);
        }

        public static IEnumerable<string> RecurseFiles(string path)
        {

            foreach (var file in Directory.GetFiles(path))
            {
                yield return file;
            }

            foreach (var dir in Directory.GetDirectories(path))
            {
                foreach (var subfile in RecurseFiles(Path.Combine(path, dir)))
                {
                    yield return subfile;
                }
            }
        }

        public static string GetRelativePath(string basePath, string fullPath)
        {
            if (!fullPath.Substring(0, basePath.Length).Equals(basePath, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new Exception("Full path doesn't start with base path");
            }

            string ret = fullPath.Substring(basePath.Length);
            ret = ret.TrimStart(Path.DirectorySeparatorChar);

            return ret;
        }
    }
}
