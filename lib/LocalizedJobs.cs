﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace armasync.lib
{
    public class LocalizedJobs : IDisposable
    {
        private readonly Thread[] _workers;
        private readonly Queue<Action> _jobs = new Queue<Action>();
        private volatile bool _running = true;
        private int _errors = 0;

        public int JobCount
        {
            get
            {
                lock (_jobs)
                    return _jobs.Count;
            }
        }

        public int JobErrors
        {
            get
            {
                return _errors;
            }
        }

        public LocalizedJobs(int workers)
        {
            var threads = new List<Thread>();
            for (int i = 0; i < workers; ++i)
            {
                var thread = new Thread(Thread_Worker);
                thread.Name = string.Format("Worker-{0}", i);
                thread.Start();
                threads.Add(thread);
            }
            _workers = threads.ToArray();
        }

        public LocalizedJobs()
            : this(Environment.ProcessorCount / 2 + 1)
        { }

        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                this.Join();
            }
        }

        public void Queue(Action job)
        {
            if (_disposed)
                throw new ObjectDisposedException("LocalizedJobs");

            lock (_jobs)
            {
                _jobs.Enqueue(job);
                Monitor.Pulse(_jobs);
            }
        }

        private void Join()
        {
            _running = false;
            lock(_jobs)
            {
                Monitor.PulseAll(_jobs);
            }
            foreach(var worker in _workers)
            {
                worker.Join();
            }
        }

        private Action GetNextJob()
        {
            lock (_jobs)
            {
                if (_jobs.Count == 0)
                {
                    if (!_running)
                        return null;

                    Monitor.Wait(_jobs);

                    if (_jobs.Count == 0)
                        return null;
                }
                return _jobs.Dequeue();
            }
        }

        private void Thread_Worker()
        {
            Action job;
            while ((job = this.GetNextJob()) != null)
            {
                try
                {
                    job();
                }
                catch(Exception e)
                {
                    Interlocked.Increment(ref _errors);
                    Console.Error.WriteLine("Error running job: {0}", e);
                }
            }
        }

    }
}
