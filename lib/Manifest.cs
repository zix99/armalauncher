using System;
using System.IO;
using System.Collections.Generic;

namespace armasync.lib
{
    public class Manifest
    {
        private readonly List<Fragment> _fragments = new List<Fragment> ();
        private readonly List<string> _purifyPaths = new List<string>();

        public readonly string MirrorRoot;

        public IEnumerable<Fragment> Fragments
        {
            get
            {
                return _fragments;
            }
        }

        public IEnumerable<string> PurifiedPaths
        {
            get
            {
                return _purifyPaths;
            }
        }

        public Manifest (Stream stream)
        {
            var reader = new StreamReader (stream);

            int lineNum=0;
            while(!reader.EndOfStream)
            {
                string line = reader.ReadLine ().Trim ();
                lineNum++;

                string[] parts = line.Split ('\t');

                try
                {
                    switch (parts[0])
                    {
                        case "R":
                            {
                                if (this.MirrorRoot != null)
                                    throw new Exception("Only one root can be specified");
                                this.MirrorRoot = Pathing.Sanitize(parts[1]);
                            }
                            break;
                        case "A": //Add
                            {
                                string filename = Pathing.Sanitize(parts[1]);
                                long length = long.Parse(parts[2]);
                                long timestamp = long.Parse(parts[3]);
                                var compressMode = GetCompressionFromString(parts[4]);
                                string hash = parts[5];

                                _fragments.Add(new Fragment(filename, length, timestamp, compressMode, hash));
                            }
                            break;
                        case "P": //Purify
                            {
                                string path = Pathing.Sanitize(parts[1]);
                                _purifyPaths.Add(path);
                            }
                            break;
                        default:
                            throw new Exception("Unknown manifest line type");
                    }
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Unknown manifest line ({0}): {1}", lineNum, e.Message);
                }
            }
        }

        private FragmentCompression GetCompressionFromString(string mode)
        {
            switch (mode.ToLowerInvariant())
            {
                case "gz":
                    return FragmentCompression.GZip;
                case "bz2":
                    return FragmentCompression.BZip2;
            }
            return FragmentCompression.Raw;
        }

    }
}

