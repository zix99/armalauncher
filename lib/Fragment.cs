using System;
using System.IO;

namespace armasync.lib
{
    public class Fragment
    {
        private static readonly DateTime EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);

        public readonly string FragmentPath;
        public readonly long Length;
        public readonly long Timestamp;
        public FragmentCompression CompressMode;
        public readonly string Hash;

        public DateTime ModifiedTime
        {
            get
            {
                return EPOCH.AddSeconds(this.Timestamp);
            }
        }

        public string BasePath
        {
            get
            {
                return Path.GetDirectoryName (this.FragmentPath);
            }
        }

        public string Filename
        {
            get
            {
                return Path.GetFileName (this.FragmentPath);
            }
        }

        public Fragment (string path, long length, long timestamp, FragmentCompression compressMode, string hash)
        {
            this.FragmentPath = path;
            this.Length = length;
            this.Timestamp = timestamp;
            this.CompressMode = compressMode;
            this.Hash = hash;
        }
    }
}

