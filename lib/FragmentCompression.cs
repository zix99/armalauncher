﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace armasync.lib
{
    public enum FragmentCompression
    {
        Raw=0,
        GZip,
        BZip2
    }
}
