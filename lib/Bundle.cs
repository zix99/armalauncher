﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace armasync.lib
{
    class Bundle
    {
        public class BundleOption
        {
            public readonly string Key;
            public readonly string Description;
			public readonly string StartScript;
            public BundleOption(string key, string desc, string startScript)
            {
                this.Key = key;
                this.Description = desc;
				this.StartScript = startScript;
            }
        }

        private readonly List<BundleOption> _options = new List<BundleOption>();

        public readonly string Prompt = "Please choose your launch bundle:";

        public int Count
        {
            get
            {
                return this._options.Count;
            }
        }

        public IEnumerable<BundleOption> Options
        {
            get
            {
                return this._options;
            }
        }

        public BundleOption this[int i]
        {
            get { return _options[i]; }
        }

        public Bundle(Stream stream)
        {
            var reader = new StreamReader(stream);

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine().Trim();
                string[] parts = line.Split('\t');

                try
                {
                    switch (parts[0])
                    {
                        case "O": //option
                            _options.Add(new BundleOption(parts[1], parts[2], parts.ElementAtOrDefault(3)));
                            break;
                        case "P": //prompt
                            this.Prompt = parts[1];
                            break;
                        default:
                            throw new Exception("Unknown bundle line");
                    }
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Error parsing bundle file: {0}", line);
                }
            }
        }

        
    }
}
