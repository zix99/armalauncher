﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ionic.Zlib;
using Ionic.BZip2;
using System.IO;

namespace armasync.lib
{
    public static class Decompression
    {
        public static void DecompressGZip(string filename, string toFile, Action<long> callback = null)
        {
            DecompressInternal(filename, toFile, fn => new GZipStream(File.OpenRead(fn), CompressionMode.Decompress), callback);
        }

        public static void DecompressBZip2(string filename, string toFile, Action<long> callback = null)
        {
            DecompressInternal(filename, toFile, fn => new BZip2InputStream(File.OpenRead(fn)), callback);
        }

        private static void DecompressInternal(string filename, string toFile, Func<string, Stream> decompressionProvider, Action<long> callback)
        {
            if (File.Exists(toFile))
                File.Delete(toFile);

            long totalBytes = 0;
            int consoleTick = 0;
            byte[] buffer = new byte[8192];
            using (var stream = decompressionProvider(filename))
            using (var ostream = File.OpenWrite(toFile))
            {
                int n;
                while ((n = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ostream.Write(buffer, 0, n);
                    totalBytes += n;

                    consoleTick++;
                    if (consoleTick > 100)
                    {
                        if (callback != null)
                            callback(totalBytes);
                        consoleTick = 0;
                    }
                }
            }

            File.Delete(filename);
        }
    }
}
