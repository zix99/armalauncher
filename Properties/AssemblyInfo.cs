using System.Reflection;
using System.Runtime.CompilerServices;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.
[assembly: AssemblyTitle ("ArmaSync")]
[assembly: AssemblyDescription ("Arma 3 Mod Synchronization")]
[assembly: AssemblyConfiguration ("")]
[assembly: AssemblyCompany ("ZDyn")]
[assembly: AssemblyProduct ("")]
[assembly: AssemblyCopyright ("ZDyn; MIT License 2014")]
[assembly: AssemblyTrademark ("")]
[assembly: AssemblyCulture ("")]
