#!/usr/bin/env python
import os, os.path
import sys
import struct
import gzip, bz2
import hashlib

def log(msg):
        sys.stderr.write("LOG:" + msg + "\n")
        sys.stderr.flush()

def getStreamDetails(f):
        size = 0
        sha = hashlib.sha1()
        while True:
                buf = f.read(1024)
                if not buf: break
                sha.update(buf)
                size += len(buf)
        return (size, sha.hexdigest())

def getBzip2Details(path):
        with bz2.BZ2File(path, 'rb') as f:
                return getStreamDetails(f)


def getGzipDetails(path):
        with gzip.GzipFile(path, 'rb') as f:
                return getStreamDetails(f)

def getFileDetails(path):
        return getStreamDetails(open(path, 'rb'))

def printFileSpec(fullpath):
        stat = os.stat(fullpath)
        filesize = stat.st_size
        compress = ""
        sha = ''

        if fullpath[-3:] == ".gz":
                filesize, sha = getGzipDetails(fullpath)
                fullpath = fullpath[:-3]
                compress = "gz"
        elif fullpath[-4:] == ".bz2":
                filesize, sha = getBzip2Details(fullpath)
                fullpath = fullpath[:-4]
                compress = "bz2"
        else:
                compress = "raw"
                filesize, sha = getFileDetails(fullpath)

        print "A\t%s\t%d\t%d\t%s\t%s" % (fullpath, filesize, stat.st_mtime, compress, sha)

def printSubManifest(root):
        log("Submanifest for %s" % root)
        for line in open(os.path.join(root, ".manifest")):
                parts = line.strip().split('\t')
                if len(parts) > 0:
                        if parts[0] == 'A':
                                fullpath = os.path.join(root, parts[1])
                                filesize = int(parts[2])
                                mtime = int(parts[3])
                                compress = parts[4]
                                sha = parts[5]
                                print "A\t%s\t%d\t%d\t%s\t%s" % (fullpath, filesize, mtime, compress, sha)
                        elif parts[0] == 'R':
                                pass #ignore
                        else:
                                log("Unsupported manifest line %s" % parts[0])

def walkPath(root = "", depth = 0):
        log("Walk: %s" % root)

        #Ignore this folder
        if os.path.isfile(os.path.join(root, ".ignore")):
                return

        #Mark as pure
        if os.path.isfile(os.path.join(root, ".pure")):
                print "P\t%s" % (root)

        #Precomputed manifest
        if depth > 0: #Dont use .manifest on root
                manifestPath = os.path.join(root, ".manifest")
                if os.path.isfile(manifestPath):
                        printSubManifest(root)
                        return

        #Walk structure
        for item in os.listdir(root or "./"):
                fullpath = os.path.join(root, item)
                if os.path.isfile(fullpath) and not item.startswith('.'):
                        printFileSpec(fullpath)
                elif os.path.isdir(fullpath) or os.path.islink(fullpath):
                        walkPath(fullpath, depth + 1)

def main(args):
        if len(args) != 1:
                print "manifestgen.py <root>"
                return

        root = args[0]
        outPath = os.path.join(root, ".manifest")
        log("Redirecting stdout to: %s" % outPath)
        sys.stdout = open(outPath, 'w')

        print "R\t%s" % root
        os.chdir(root)
        walkPath()

if __name__ == "__main__":
        main(sys.argv[1:])

