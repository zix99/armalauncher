﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace armasync
{
    internal static class Config
    {
        public static readonly string MIRROR = "http://update.arma.zdyn.net";

        public static readonly string MIRROR_MANIFEST = "manifest";

        public static readonly string MIRROR_BUNDLE = "bundles";

        public static readonly string[] SEARCH_PATHS = new string[]{
         "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Arma 3"
        };

        public static readonly string LAUNCH_CMD = "zdynlauncher.bat";

        public static bool ValidatePath(string path)
        {
            return File.Exists(Path.Combine(path, "arma3.exe"))
                || File.Exists(Path.Combine(path, "arma3server.exe"))
                || File.Exists(Path.Combine(path, "arma3server")); //unix
        }
    }
}
