using System;
using System.Linq;
using armasync.lib;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace armasync
{
    public class Syncer
    {
        private readonly string _path;
        private readonly string _mirror;
        private readonly string _manifestName;

        private readonly HttpDownloader _downloader;

        protected string ManifestUrl
        {
            get
            {
                return string.Format ("{0}/{1}", _mirror, _manifestName);
            }
        }

        public Syncer (string mirror, string manifestName, string path)
        {
            _path = path;
            _mirror = mirror;
            _manifestName = manifestName;
            _downloader = new HttpDownloader ();
            _downloader.UpdateProgress += HandleUpdateProgress;
        }

        void HandleUpdateProgress (string url, long received, long total, double speed)
        {
            Quicky.WriteProgress (received, total, (long)speed, 4);
        }

        public bool Update()
        {
            Manifest manifest;
            string manifestContent = _downloader.DownloadText(this.ManifestUrl);
            if (string.IsNullOrEmpty(manifestContent))
            {
                Console.Error.WriteLine("Failed to download manifest");
                return false;
            }

            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(manifestContent)))
            {
                manifest = new Manifest(ms);
            }

            //Collect full list of things we have to update
            var updateList = new List<Fragment>();
            foreach (var frag in manifest.Fragments)
            {
                Console.WriteLine("Validating {0}...", frag.FragmentPath);

                string fullPath = Path.Combine(_path, Pathing.Normalize(frag.FragmentPath));
                var fi = new FileInfo(fullPath);

                //Do we need an update?
                if (ShouldUpdateFile(frag, fi))
                {
                    updateList.Add(frag);
                }
            }

            //Update fragments in the manifest
            long totalBytes = updateList.Sum(x => x.Length);
            long completedBytes = 0;
            DateTime startTime = DateTime.Now;

            Console.WriteLine();
            Console.WriteLine("Need to update {0} file(s) ({1})...", updateList.Count, Quicky.FormatBytes(totalBytes));

            using (var jobs = new LocalizedJobs())
            {
                for (int i = 0; i < updateList.Count; ++i)
                {
                    var frag = updateList[i];

                    // Get paths
                    string fullPath = Path.Combine(_path, Pathing.Normalize(frag.FragmentPath));
                    string fullUrl = string.Format("{0}/{1}{2}", _mirror, manifest.MirrorRoot, frag.FragmentPath);

                    // Calculate ETA
                    DateTime? eta = null;
                    double passedTime = (DateTime.Now - startTime).TotalSeconds;
                    if (passedTime > 15.0 && completedBytes > 1000)
                    {
                        double averageRate = completedBytes / passedTime;
                        eta = DateTime.Now + TimeSpan.FromSeconds((totalBytes - completedBytes) / averageRate);
                    }

                    Console.WriteLine("Updating {0}...", frag.FragmentPath);
                    Console.WriteLine("    [{0} of {1}] ({2}/{3}) ETA {4}", Quicky.FormatBytes(completedBytes), Quicky.FormatBytes(totalBytes), i + 1, updateList.Count, eta != null ? eta.Value.ToShortTimeString() : "???");

                    string baseDir = Path.GetDirectoryName(fullPath);
                    if (!Directory.Exists(baseDir))
                        Directory.CreateDirectory(baseDir);

                    if (frag.CompressMode == FragmentCompression.GZip
                        && _downloader.Download(fullUrl + ".gz", fullPath + ".gz"))
                    {
                        Quicky.ClearCurrentLine("    Decompressing...");
                        jobs.Queue(() =>
                        {
                            Decompression.DecompressGZip(fullPath + ".gz", fullPath);
                            File.SetLastWriteTime(fullPath, frag.ModifiedTime);
                        });
                    }
                    else if (frag.CompressMode == FragmentCompression.BZip2
                        && _downloader.Download(fullUrl + ".bz2", fullPath + ".bz2"))
                    {
                        Quicky.ClearCurrentLine("    Decompressing...");
                        jobs.Queue(() =>
                        {
                            Decompression.DecompressBZip2(fullPath + ".bz2", fullPath);
                            File.SetLastWriteTime(fullPath, frag.ModifiedTime);
                        });
                    }
                    else if (frag.CompressMode == FragmentCompression.Raw
                        && _downloader.Download(fullUrl, fullPath))
                    {
                        //Normal (Nothing)
                        File.SetLastWriteTime(fullPath, frag.ModifiedTime);
                        Quicky.ClearCurrentLine("    OK.");
                    }
                    else
                    {
                        Console.Error.WriteLine("    FAILED");
                        return false;
                    }

                    //If at any time there was an error, abort the mission!
                    if (jobs.JobErrors > 0)
                    {
                        Console.Error.WriteLine("    Decompression failed!");
                        return false;
                    }

                    completedBytes += frag.Length;
                    Console.Write("\n\n");
                }

                Console.WriteLine("Waiting for decompression to finish...");
            }

            //Run purification (Delete all files within a path that aren't on the manifest)
            foreach (var purePath in manifest.PurifiedPaths)
            {
                Console.WriteLine("Purifying {0}...", purePath);

                string fullPath = Path.Combine(_path, Pathing.Normalize(purePath));

                try
                {
                    if (Directory.Exists(fullPath))
                    {
                        //Walk the files in the path, and if one of them isn't in the manifest, kill it
                        foreach (var filename in Pathing.RecurseFiles(fullPath))
                        {
                            string relPath = Pathing.GetRelativePath(_path, filename);
                            if (!manifest.Fragments.Any(x => string.Equals(Pathing.Normalize(x.FragmentPath), relPath, StringComparison.OrdinalIgnoreCase))) // ignore-case cause stupid windows
                            {
                                Console.WriteLine("  Removing {0}", relPath);
                                File.Delete(filename);
                            }
                        }

                        //If after a path was purified, it's now empty, remove the path
                        if (!Pathing.RecurseFiles(fullPath).Any())
                            Directory.Delete(fullPath, true);
                    }
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e);
                }

            }

            Console.WriteLine("Done");

            return true;
        }

        private bool ShouldUpdateFile(Fragment frag, FileInfo fi)
        {
            if (!fi.Exists)
                return true;

            if (fi.Length != frag.Length)
                return true;

            if (fi.LastWriteTime != frag.ModifiedTime)
            {
                Console.Write("Timestamp mismatch, comparing sha... ");
                if (Hasher.ComputeFileSha(fi.FullName) != frag.Hash)
                {
                    Console.WriteLine("MISMATCH");
                    return true;
                }
                
                try
                {
                    File.SetLastWriteTime(fi.FullName, frag.ModifiedTime);
                    Console.WriteLine("OK");
                }
                catch (Exception e)
                {
                    Console.WriteLine("OK. Error updating modified time.");
                }
            }
            return false;
        }
    }
}

