﻿using armasync.lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace armasync
{
    class BundleSelector
    {
        private readonly string _mirrorUrl;
        private readonly string _bundleFile;

        protected string BundleUrl
        {
            get { return string.Format("{0}/{1}", _mirrorUrl, _bundleFile); }
        }

        public BundleSelector(string mirrorUrl, string bundleFile)
        {
            _mirrorUrl = mirrorUrl;
            _bundleFile = bundleFile;
        }

        private Bundle _bundle;
        public Bundle Bundle
        {
            get
            {
                return _bundle ?? (_bundle = this.DownloadBundle());
            }
        }

        public Bundle.BundleOption PromptForManifestName()
        {
            if (this.Bundle == null)
            {
                Console.WriteLine("No bundle options");
                return null;
            }

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine(this.Bundle.Prompt);

                for (int i = 0; i < this.Bundle.Count; ++i)
                {
                    Console.WriteLine("  [{0}] {1}", i, this.Bundle[i].Description);
                }

                Console.Write("Enter option [0]: ");
                string read = Console.ReadLine().Trim();

                if (read == string.Empty) //default
                    return this.Bundle[0];

                int idx;
                if (int.TryParse(read, out idx) && idx >= 0 && idx < this.Bundle.Count)
                {
                    return this.Bundle[idx];
                }
            }
        }


        private Bundle DownloadBundle()
        {
            Console.WriteLine("Downloading bundle options...");
            var http = new HttpDownloader();
            string content = http.DownloadText(this.BundleUrl);
            if (content == null)
                return null;

            using(var ms = new MemoryStream(Encoding.UTF8.GetBytes(content)))
            {
                return new Bundle(ms);
            }
        }
    }
}
