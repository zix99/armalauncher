﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace armasync
{
    public class Platform
    {

        protected Platform()
        { }

        private static Platform _instance;
        public static Platform Instance
        {
            get
            {
                return _instance ?? (_instance = new Platform());
            }
        }

        public bool IsUnix
        {
            get
            {
                int p = (int)Environment.OSVersion.Platform;
                return p == 4 || p == 6 || p == 128;
            }
        }

    }
}
