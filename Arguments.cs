﻿using Mono.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace armasync
{
    public class Arguments
    {
        private readonly OptionSet _options;

        public bool ShowHelp { get; private set; }

        public string Target{get; private set;}

        public string ManifestNameOverride { get; private set; }

        public bool NoLaunch { get; private set; }

        public bool UpdateOnly { get; private set; }

        protected Arguments(string[] args)
        {

            _options = new OptionSet()
            {
                {"h|help", "Shows help", v => this.ShowHelp = true},
                {"t|target=", "Sets the target install path", v => this.Target = v},
                {"n|nolaunch", "Dont launch after update", v => NoLaunch = true},
                {"m|manifest=", "Manifest name override for specifying versions", v => this.ManifestNameOverride = v},
                {"u|self-update", "Only run self-update, not anything else", v => this.UpdateOnly = true}
            };
            _options.Parse(args);
        }

        private static Arguments _instance;
        public static Arguments Instance
        {
            get
            {
                return _instance ?? (_instance = new Arguments(Environment.GetCommandLineArgs()));
            }
        }

        public void WriteHelp(TextWriter o)
        {
            var asmInfo = Assembly.GetEntryAssembly().GetName();

            o.WriteLine("{0} [args]", asmInfo.Name);
            o.WriteLine("  Version: {0}", asmInfo.Version.ToString());
            o.WriteLine();
            o.WriteLine("Arguments:");
            _options.WriteOptionDescriptions(o);
        }

    }
}
