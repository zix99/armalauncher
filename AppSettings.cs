﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace armasync
{
    public class AppSettings
    {
        private readonly string _path;

        public string GamePath
        {
            get;
            set;
        }

        protected AppSettings(string filePath)
        {
            _path = filePath;
            TryLoadSettings();
        }

        private static AppSettings _instance;
        public static AppSettings Instance
        {
            get
            {
                return _instance ??
                    (_instance = new AppSettings(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "arma-zdyn.dat")));
            }
        }

        private void TryLoadSettings()
        {
            if (File.Exists(_path))
            {
                using (var reader = new StreamReader(this._path))
                {
                    this.GamePath = reader.ReadLine();
                }
            }
        }

        public void Save()
        {
            Directory.CreateDirectory (Path.GetDirectoryName (this._path));
            using (var writer = new StreamWriter(this._path))
            {
                writer.WriteLine(this.GamePath);
            }
        }

    }
}
